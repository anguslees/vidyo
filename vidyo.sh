#!/bin/sh

tag=${1:-latest}
: ${UID:=$(id -u)}
videodev=$(set -- /dev/video*; echo $1)

exec docker run --rm --name=vidyo \
 -u $UID:video \
 -e DISPLAY \
 --device $videodev:/dev/video0:rw \
 --device /dev/dri:/dev/dri:rw \
 -v /tmp/.X11-unix:/tmp/.X11-unix \
 -v /run/user/$UID/pulse/native:/run/pulse/native \
 -v /etc/machine-id:/etc/machine-id:ro \
 -v $HOME/.vidyo:/home/user/.vidyo \
 quay.io/anguslees/vidyo:$tag
