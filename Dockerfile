FROM ubuntu:14.04
MAINTAINER Angus Lees <gus@inodes.org>

# Run me with:
#  docker run --rm --name=vidyo -u $UID:video \
#   -e DISPLAY \
#   --device /dev/video0:/dev/video0:rw \
#   --device /dev/dri:/dev/dri:rw \
#   -v /tmp/.X11-unix:/tmp/.X11-unix \
#   -v /dev/shm:/dev/shm \
#   -v /run/user/$UID/pulse/native:/run/pulse/native \
#   -v /etc/machine-id:/etc/machine-id:ro \
#   -v $HOME/.vidyo:/home/user/.vidyo

RUN adduser --disabled-password --gecos 'User' user

RUN \
 apt-get update && \
 apt-get install -q -y --no-install-recommends wget ca-certificates pulseaudio \
 libqtwebkit4 libglu1-mesa libgl1-mesa-dri libxv1 libxrandr2 && \
 wget -q https://demo.vidyo.com/upload/VidyoDesktopInstaller-ubuntu64-TAG_VD_3_6_3_017.deb && \
 dpkg --unpack VidyoDesktopInstaller-ubuntu64-TAG_VD_*.deb && \
 apt-get install -q -y --no-install-recommends -f && \
 rm VidyoDesktopInstaller-*.deb && \
 rm -rf /var/lib/apt/lists/* && \
 echo enable-shm=no >> /etc/pulse/client.conf

# This seems to be the easiest way to disable the EULA dialog
RUN rmdir /opt/vidyo/VidyoDesktop/lic

USER user
ENV HOME=/home/user PULSE_SERVER=/run/pulse/native QT_X11_NO_MITSHM=1

CMD [ "/usr/bin/VidyoDesktop" ]
