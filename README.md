VidyoDesktop
============

Vidyo Linux client, in a container because it is fragile closed-source software
that needs just the right binary dependencies in order to work.

Assumes you're using pulseaudio, and the wrapper script uses the first
`/dev/video*` camera device (but should be obvious to adapt if you have other
needs).

Usage
-----

Install and run docker daemon.  Make sure your current user can start docker
containers, or modify the script to use sudo/whatever.

```
wget https://gitlab.com/anguslees/vidyo/raw/master/vidyo.sh
chmod +x vidyo.sh
./vidyo.sh
```

It will be slow to start the first time you run it, while docker downloads
the required layers.